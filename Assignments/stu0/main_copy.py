#
# File: lect.py
# Date: 7/3/2022
# Desc: Change line 7 to be your student Id and the exercise that you are currently
#       working on.
#


from Assignments.stu0.assignments import *
from Lectures.lec1 import *
from Assignments.stu0.iga.iga import *
from Assignments.stu0.igasubmissions.stu1.iga import *


def main():
    ex1()
    #lec1()
    #iga()
    #iga()


if __name__ == '__main__':
    main()
